1. define ERD in erd.txt
2. `npx auto-migrate db.sqlite3 < erd.txt`
3. `npx knex migrate:latest`
4. `npx erd-to-proxy < erd.txt > proxy.ts`
5. write sample data in "seed-data.ts"
6. `npx ts-node seed-data`

Select timestamp and display as local time:

```sql
select
  id
, username
, datetime(created_at,'localtime')
from user 
```