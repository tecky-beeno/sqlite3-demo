import { filter, unProxy } from 'better-sqlite3-proxy'
import { db } from './db'
import { proxy } from './proxy'

// sql: select count(*) from shopping_cart
console.log(proxy.shopping_cart.length)

// sql: select * from shopping_cart
console.log(unProxy(proxy.shopping_cart))

/* sql:
select
  user.username
, product.name as product_name
, product.price as unit_price
from shopping_cart
inner join user on user.id = shopping_cart.user_id
inner join product on product.id = shopping_cart.product_id
*/
console.log(
  proxy.shopping_cart.map(cart => {
    return {
      username: cart.user?.username,
      product_name: cart.product?.name,
      unit_price: cart.product?.price,
    }
  }),
)

// search
// sql: select ... from shopping_cart WHERE ...
let user_id = 1 // e.g. got from req.session.user.id
let total_price = 0
let items = filter(proxy.shopping_cart, { user_id }).map(cart => {
  let subtotal_price = cart.quantity * cart.product!.price
  total_price += subtotal_price
  return {
    product_name: cart.product?.name,
    unit_price: cart.product?.price,
    quantity: cart.quantity,
    subtotal_price,
  }
})
console.log({ total_price, items })

// timestamp convertion
console.log('user[1]', unProxy(proxy.user[1]))

let users = db.query(/* sql */ `
select
  id
, username
, datetime(created_at,'localtime') as signup_at
from user
`)
console.log('users:', users)

// search with timestamp
let select_user_statement = db.prepare(/* sql */ `
select
  id
, username
, datetime(created_at,'localtime') as signup_at
from user
where user.id = :id
`)

// app.get('/users/:id/,(req,res)=>{ /* use the prepared statement */ })

let user = select_user_statement.get({ id: 2 })
console.log('user:', user)

let select_cart_items = db.prepare(/* sql */ `
select
  product.id
, product.name
, product.price as unit_price
, shopping_cart.quantity
, shopping_cart.quantity * product.price as subtotal_price
, datetime(shopping_cart.created_at,'localtime') as cart_time
from shopping_cart
inner join product on product.id = shopping_cart.product_id
where shopping_cart.user_id = :user_id
`)
let user_cart_items = select_cart_items.all({ user_id: 1 })
console.log('user_cart_items:', user_cart_items)
