import { Knex } from "knex";


export async function up(knex: Knex): Promise<void> {

  if (!(await knex.schema.hasTable('user'))) {
    await knex.schema.createTable('user', table => {
      table.increments('id')
      table.text('username').notNullable()
      table.timestamps(false, true)
    })
  }

  if (!(await knex.schema.hasTable('product'))) {
    await knex.schema.createTable('product', table => {
      table.increments('id')
      table.text('name').notNullable()
      table.specificType('price', 'real').notNullable()
      table.timestamps(false, true)
    })
  }

  if (!(await knex.schema.hasTable('shopping_cart'))) {
    await knex.schema.createTable('shopping_cart', table => {
      table.increments('id')
      table.integer('user_id').unsigned().notNullable().references('user.id')
      table.integer('product_id').unsigned().notNullable().references('product.id')
      table.timestamps(false, true)
    })
  }
}


export async function down(knex: Knex): Promise<void> {
  await knex.schema.dropTableIfExists('shopping_cart')
  await knex.schema.dropTableIfExists('product')
  await knex.schema.dropTableIfExists('user')
}
