import { Knex } from "knex";


export async function up(knex: Knex): Promise<void> {
  await knex.schema.alterTable('shopping_cart', table => {
    table.integer('quantity').notNullable()
  })
}


export async function down(knex: Knex): Promise<void> {
  await knex.schema.alterTable('shopping_cart', table => {
    table.dropColumn('quantity')
  })
}
