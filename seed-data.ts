import { proxy } from './proxy'

console.log('start seeding data...')

// insert for the first time;
// update if run again (because the ID is already used)
proxy.user[1] = { username: 'alice' }
proxy.user[2] = { username: 'bob' }
proxy.user[3] = { username: 'cherry' }

proxy.product[1] = { name: 'Apple', price: 11 }
proxy.product[2] = { name: 'Banana', price: 12 }
proxy.product[3] = { name: 'Cherry', price: 13 }

proxy.shopping_cart[1] = {
  user_id: 1,
  product_id: 1,
  quantity: 2,
}
proxy.shopping_cart[2] = {
  user_id: 1,
  product_id: 2,
  quantity: 5,
}

proxy.shopping_cart[3] = {
  user_id: 3,
  product_id: 3,
  quantity: 12,
}

console.log('finished data seeding.')
