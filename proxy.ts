import { proxySchema } from 'better-sqlite3-proxy'
import { db } from './db'

export type User = {
  id?: number | null
  username: string
}

export type Product = {
  id?: number | null
  name: string
  price: number
}

export type ShoppingCart = {
  id?: number | null
  user_id: number
  user?: User
  product_id: number
  product?: Product
  quantity: number
}

export type DBProxy = {
  user: User[]
  product: Product[]
  shopping_cart: ShoppingCart[]
}

export let proxy = proxySchema<DBProxy>({
  db,
  tableFields: {
    user: [],
    product: [],
    shopping_cart: [
      /* foreign references */
      ['user', { field: 'user_id', table: 'user' }],
      ['product', { field: 'product_id', table: 'product' }],
    ],
  },
})
